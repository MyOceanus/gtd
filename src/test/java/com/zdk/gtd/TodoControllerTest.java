package com.zdk.gtd;

import com.zdk.gtd.entity.Todo;
import com.zdk.gtd.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    private TodoRepository todoRepository;
    @Autowired
    private MockMvc client;


    @BeforeEach
    void prepareData() {
        todoRepository.deleteAll();
    }


    @Test
    void should_return_todos_when_get_given_todos_in_db() throws Exception {

        Todo readBooks = new Todo("read 1000 books", false);
        Todo doSports = new Todo("do some sports", false);

        todoRepository.saveAll(List.of(readBooks,doSports));

        client.perform(MockMvcRequestBuilders.get("/todos"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(readBooks.getText()))
                .andExpect(jsonPath("$[0].done").value(readBooks.isDone()))
                .andExpect(jsonPath("$[1].text").value(doSports.getText()))
                .andExpect(jsonPath("$[1].done").value(doSports.isDone()));
    }

    @Test
    void should_create_new_todo_when_perform_post_given_new_todo() throws Exception {
        // given

        String newTodoJson = "{" +
                "\"text\":\"playGames\"}";
        // when
        client.perform(MockMvcRequestBuilders.post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newTodoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("playGames"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));
        //then
        Todo todoSaved = todoRepository.findAll().get(0);
        assertThat(todoSaved.getText(),equalTo("playGames"));
        assertThat(todoSaved.isDone(),equalTo(false));
    }

    @Test
    void should_return_todo_by_id_when_perform_get_given_todo() throws Exception {
        // given
        Todo inserted = todoRepository.save(
                new Todo("test", false)
        );

        // when
        client.perform(MockMvcRequestBuilders.get("/todos/{id}", inserted.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("test"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));

        // should
    }

    @Test
    void should_return_404_when_perform_get_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.get("/todos/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }

    @Test
    void should_return_updated_todo_when_perform_put_text_given_todo() throws Exception {
        // given
        Todo todo = todoRepository.save(new Todo("test2",false));
        String newTodoJson = "{" +
                "\"text\":\"test2\"}";


        // when
        client.perform(MockMvcRequestBuilders.put("/todos/text/{id}", todo.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(newTodoJson))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("test2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(false));

        // should
        Todo updatedTodo = todoRepository.findAll().get(0);
        assertThat(updatedTodo.getId(), equalTo(todo.getId()));
        assertThat(updatedTodo.getText(), equalTo("test2"));
        assertThat(updatedTodo.isDone(), equalTo(false));
    }

    @Test
    void should_return_updated_to_when_perform_put_done_given_todo() throws Exception {
        // given
        Todo todo = todoRepository.save(new Todo("test1",false));

        // when
        client.perform(MockMvcRequestBuilders.put("/todos/done/{id}", todo.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text").value("test1"))
                .andExpect(jsonPath("$.done").value(true));

        // should
        Todo updateTodo = todoRepository.findAll().get(0);
        assertThat(updateTodo.getId(), equalTo(todo.getId()));
        assertThat(updateTodo.getText(), equalTo("test1"));
        assertThat(updateTodo.isDone(),equalTo(true));
    }

    @Test
    void should_return_404_when_perform_put_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.put("/todos/done/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        String newTodoJson = "{" +
                "\"text\":\"test2\"}";
        client.perform(MockMvcRequestBuilders.put("/todos/text/999")
                .contentType(MediaType.APPLICATION_JSON)
                .content(newTodoJson))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
        // should
    }

    @Test
    void should_return_404_when_perform_delete_by_id_given_id_not_exist() throws Exception {
        // given

        // when
        client.perform(MockMvcRequestBuilders.delete("/todos/999"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());

        // should
    }
}
