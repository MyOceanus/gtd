package com.zdk.gtd.controller;

import com.zdk.gtd.dto.TodoRequest;
import com.zdk.gtd.dto.TodoResponse;
import com.zdk.gtd.entity.Todo;
import com.zdk.gtd.service.TodoService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> list() {
        return todoService.list();
    }

    @PostMapping
    public TodoResponse create(@RequestBody TodoRequest todoRequest){
        return todoService.save(todoRequest);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        todoService.deleteById(id);
    }

    @PutMapping("/done/{id}")
    public TodoResponse updateDone(@PathVariable Integer id){
        return todoService.updateDone(id);
    }

    @PutMapping("/text/{id}")
    public TodoResponse update(@PathVariable Integer id , @RequestBody TodoRequest todoRequest){
        return  todoService.update(id,todoRequest);
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Integer id){
        return todoService.getById(id);
    }
}
