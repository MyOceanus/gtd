package com.zdk.gtd.service;

import com.zdk.gtd.dto.TodoRequest;
import com.zdk.gtd.dto.TodoResponse;
import com.zdk.gtd.entity.Todo;
import com.zdk.gtd.exception.NotFindByIdException;
import com.zdk.gtd.mapper.TodoMapper;
import com.zdk.gtd.repository.TodoRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class TodoService {

    private final TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoResponse> list() {
        return todoRepository.findAll()
                .stream()
                .map(TodoMapper::toDto)
                .collect(toList());
    }

    public TodoResponse save(TodoRequest todoRequest) {
        Todo saved = todoRepository.save(TodoMapper.toEntity(todoRequest));
        return TodoMapper.toDto(saved);
    }

    public void deleteById(Integer id) {
        findById(id);
        todoRepository.deleteById(id);
    }

    public TodoResponse updateDone(Integer id) {
        Todo updateDoneTodo = findById(id);
        updateDoneTodo.setDone(!updateDoneTodo.isDone());
        todoRepository.save(updateDoneTodo);
        return TodoMapper.toDto(updateDoneTodo);
    }

    public TodoResponse update(Integer id, TodoRequest updateTodo) {
        Todo todo = findById(id);
        todo.setText(updateTodo.getText());
        todoRepository.save(todo);
        return TodoMapper.toDto(todo);
    }

    private Todo findById(Integer id){
        return todoRepository.findById(id).orElseThrow(NotFindByIdException::new);
    }

    public TodoResponse getById(Integer id) {
        return TodoMapper.toDto(todoRepository.findById(id).orElseThrow(NotFindByIdException::new));
    }
}
