package com.zdk.gtd.mapper;

import com.zdk.gtd.dto.TodoRequest;
import com.zdk.gtd.dto.TodoResponse;
import com.zdk.gtd.entity.Todo;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoResponse toDto(Todo todo){
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todo,todoResponse);
        return todoResponse;
    }

    public static Todo toEntity(TodoRequest todoRequest) {
        Todo todo = new Todo();
        BeanUtils.copyProperties(todoRequest,todo);
        return todo;
    }
}
